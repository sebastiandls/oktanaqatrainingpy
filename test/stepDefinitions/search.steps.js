import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import locationPage from '../pageobjects/location.page';
import searchResultPage from '../pageobjects/searchResult.page';
import restaurantPage from '../pageobjects/restaurant.page';

defineSupportCode(function({ When, Then }) {
    
  //scenario 1
  When(/^I select "([^"]*)" as the department$/, function(department) {
      homePage.setDepartment(department)
    })

    When(/^I enter "([^"]*)" as the address$/, function(address) {
      homePage.setAddress(address)
    })

    When(/^I search the restaurants$/, function() {
      homePage.clickBuscarButton()
    })

    When(/^I confirm my location$/, function() {
      locationPage.clickConfirmButton()
    })

    Then(/^I am taken to the restaurants list$/, function () {
      expect(searchResultPage.getResultList()).to.be.equal(true)
    })

    Then(/^I can see restaurants in the list$/, function () {
      expect(searchResultPage.getRestaurantData()).to.be.equal(true)    
    })

    //scenario 2 

    When(/^I enter "([^"]*)" as restaurant$/, function(restaurant) {
      homePage.setRestaurant(restaurant)
    }) 
    
    When(/^I filter restaurants that accept credit cards$/, function() {
      searchResultPage.filterCreditCard()
    }) 
     
    When(/^I order restaurants by better rating$/, function() {
      searchResultPage.filterRating()
    }) 

    When(/^I select the first restaurant$/, function() {
      searchResultPage.selectFirst()
    }) 

    Then(/^I print the restaurant discount$/, function() {
      expect(restaurantPage.printDiscount()).to.be.true    
    }) 
    
    Then(/^I print the price$/, function() {
      expect(restaurantPage.printPrice()).to.be.true    
    }) 

    //Scenario 3

    Then(/^I can see all restaurants are correct$/, function () {
        expect(searchResultPage.checkRestaurantName()).to.be.true    
    })
    
    //Scenario 4

    When(/^I filter restaurants by "([^"]*)"$/, function(filter) {
      searchResultPage.filterComidas(filter)
    }) 

    Then(/^I print the restaurants information$/, function () {
      expect(searchResultPage.printRestaurantInformation()).to.be.true    
  })
    
    
   })