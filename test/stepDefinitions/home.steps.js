import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';

defineSupportCode(function({ Given }) {
  Given(/^I am on the home page$/, function() {
    homePage.open()
  })

  Given(/^I am on a restaurant list for an address$/, function(table) {
    homePage.open()

    const hashes = table.hashes()
    let department = hashes[0]['department']
    let address = hashes[0]['address']

    homePage.searchByAddress(department, address)
  })
})
