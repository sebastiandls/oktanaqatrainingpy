import { defineSupportCode } from 'cucumber'
import searchResultPage from '../pageobjects/searchResult.page';
import restaurantPage from '../pageobjects/restaurant.page';
import { World } from '../World';

defineSupportCode(function({ When, Then }) {
    
    When(/^I select "([^"]*)" restaurant$/, function(restaurantName) {
      searchResultPage.selectRestaurantName(restaurantName)
    })

    When(/^I add "([^"]*)" to the order$/, function(food) {
      browser.pause(800)
      restaurantPage.addProductToOrder(food)
    })

    When(/^I change the amount of "([^"]*)" to "([^"]*)"$/, function(food, number) {
      restaurantPage.changeAmount(food, number)
    })

    Then(/^I see all menus under "Mi pedido"$/, function() {
      World.listOfFood = restaurantPage.getMenusUnderMisPedidos()
      expect(restaurantPage.getMenusUnderMisPedidos()).to.deep.equal(World.listOfFood)
    })

    Then(/^I see the subtotal is "([^"]*)"$/, function(price) {
      expect(restaurantPage.getSubTotal()).to.be.equal(price)
    })

    Then(/^I see the total is "([^"]*)"$/, function(price) {
      expect(restaurantPage.getTotal()).to.be.equal(price)
    })

    When(/^I select the first "([^"]*)" restaurant$/, function(state) {
      searchResultPage.selectFirstRestaurant(state)
    })

    When(/^I add a meal to the order$/, function() {
      restaurantPage.selectFirstMeal()
    })
  })