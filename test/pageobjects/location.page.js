import Page from './page'

class LocationPage extends Page {

  get confirmButton ()   { return browser.element('#confirm') }


  switchToLoginIframe () {
    this.iFrame.waitForVisible()
    let loginFrame = this.iFrame.element().value
    browser.frame(loginFrame)
  }

  switchBackFromIframe () {
    browser.frameParent()
  }

  clickConfirmButton () {
    //Aca me gustaria cambiar el .pause pero no puedo poner un waitForVisible() porque es posible que nunca aparezca y se tranca todo
    //Pero no se cual seria una alternativa correcta.
    browser.pause(800)
    if(this.confirmButton.isVisible()) {
      this.confirmButton.click()
    }
  }

}

export default new LocationPage()
