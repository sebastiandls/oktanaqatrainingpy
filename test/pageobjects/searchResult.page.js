import Page from './page'
import { World } from '../World';
import opensLaterPage from './opensLater.page';

class searchResultPage {

  get resultList ()             { return browser.element('#resultList') }
  get restaurantData ()         { return browser.elements('.restaurantData') }
  get restaurantNames ()        { return browser.elements('.restaurantData header h3') }
  get restaurantName ()         { return browser.element('header h3 a.arrivalName') }
  get restaurantOpen ()         { return browser.element('header h3[class=" "]') }
  get restaurantClosed ()       { return browser.element('header h3.withLabel') }
  get restaurantClosedLabel ()  { return browser.element('header h3.withLabel label.closed') }
  get restaurantRating ()       { return browser.element('div span.ranking i.rating-points') }
  get restaurantAddress ()      { return browser.element('span.restaurantInfo span.address') }
  get restaurantTime()          { return browser.element('span.infoFooter div.time i.delTime') }
  get restaurantDelivery()      { return browser.element('span.infoFooter div.shipping i') }
  get restaurantButton ()       { return browser.elements('a.button.open.arrivalButton') }
  get filterTarjeta ()          { return browser.element('ul li a[data-key=online_payment]') }
  get filterDropdown ()         { return browser.element('a.dropdown.withDrop') }
  get filterRating ()           { return browser.element('ul#drop1 li[value="rating"] a[rel="nofollow"]') }
  get confirmButton ()          { return browser.element('div#confirmWrapperModal a#lnkConfirmPop.button.linker')}
  get filterComidasUl ()        { return browser.element('ul#foodCategories') }
  get filterComidasElement ()   { return browser.elements('ul#foodCategories li a span') }


  getResultList () {
    this.resultList.waitForVisible()
    if (this.resultList.isVisible) { return true }
  }
  getRestaurantData () {
    this.restaurantData.waitForVisible()
    if (this.restaurantData.isVisible) { return true }
  }

  selectRestaurantName(restaurantName){
      let restaurant
      for(let i = 0; i < this.restaurantNames.value.length; i++){
          restaurant = this.restaurantNames.value[i].getText()
          if(restaurant === restaurantName){
            this.restaurantNames.value[i].click()
            opensLaterPage.clickConfirmButton()
            break
          }
      }
      opensLaterPage.clickConfirmButton()
    }

    filterCreditCard() {
      this.filterTarjeta.waitForVisible()
      this.filterTarjeta.click()
    }

    filterComidas(filter) {
      for(let i = 0; i < this.filterComidasElement.value.length; i++){
        let filterComidasText = this.filterComidasElement.value[i].getText()
        let filterComidasElement = this.filterComidasElement.value[i]
        if(filterComidasText === filter){
          filterComidasElement.click()
        }
      }
    }

    filterRating() {
      this.filterDropdown.waitForVisible
      this.filterDropdown.click()
      this.filterRating.waitForVisible
      this.filterRating.click
    }

    selectFirst() {
      this.restaurantNames.waitForVisible()
      this.restaurantNames.value[0].click() 
    }

    checkRestaurantName() { //usar global para guardar gourmet y usar un for por todos los restaurantes y compararlo con el gourmet
      let restaurantName
      let check = true

      for(let i = 0; i < this.restaurantNames.value.length; i++){
          restaurantName = this.restaurantNames.value[i].getText()
          if(restaurantName.includes(World.restaurant)){
            continue
          }else{
            check = false
          }
      }
      return check
    }

    printRestaurantInformation() {
      let txt = "\n \n"
    
      for( var card of this.restaurantData.value){

        let nombre = browser.elementIdElement(card.value.ELEMENT, this.restaurantName.selector).getText()
        let rating = browser.elementIdElement(card.value.ELEMENT, this.restaurantRating.selector).getText()
        let address = browser.elementIdElement(card.value.ELEMENT, this.restaurantAddress.selector).getText()
        let time = browser.elementIdElement(card.value.ELEMENT, this.restaurantTime.selector).getText()
        let delivery = browser.elementIdElement(card.value.ELEMENT, this.restaurantDelivery.selector).getText()

        //NOMBRE
        txt += "Restaurant name: " + nombre + "\n"

        //IS IT OPEN
        txt += "Is it open: "

        try { //Tuve que usar un try catch porque cuando no exisitia explotaba todo
        if(browser.elementIdElement(card.value.ELEMENT, this.restaurantClosed.selector).getText() !== ''){
          txt += "No \n"
        }
        } catch (error) {
          txt += "Yes \n"
        }

        //RESTAURANT RATING
        txt += "Restaurant rating: " + rating + "\n"

        //RESTAURANT ADDRESS
        txt += "Restaurant address: " + address + "\n"

        //TIME OF DELIVERY
        txt += "Time of delivery: " + time + "\n"

        //IS DELIVERY FREE
        txt += "Is delivery free: "
        if(delivery === "Gratis"){
          txt += "Yes \n"
        } else{
          txt += "No, " + delivery + "\n"
        }
        txt += "\n \n"

        
      }
      console.log(txt)
      return true
    }

    selectFirstRestaurant(state) {
      if ( state === "open" ){
        this.selectFirst() //tendria que tirar error si no hay ningun restaurante abierto
      }else if ( state === "still not open") {
        //Fijarme que tenga label, y que diga x

          browser.pause(5000)
        
      }else if ( state === "closed") { //Cerrado por hoy
        for( var card of this.restaurantData.value){
          try { //Tuve que usar un try catch porque cuando no exisitia explotaba todo
            if(browser.elementIdElement(card.value.ELEMENT, this.restaurantClosed.selector).getText() !== ''){
              txt += "No \n"
            }
            } catch (error) {
              txt += "Yes \n"
            }
       }
      }
    }

}
export default new searchResultPage()