import Page from './page'

class OpensLaterPage extends Page {
//Esto creo que no anda todavia pero no lo puedo arreglar cuando los restaurantes estan abiertos
//No se si es necesario crear otra .page como location ya que es el mismo tipo de popup pero menos importante
  
  get openLaterButton() { return browser.element("a#lnkConfirmPop.button.linker") }

  switchToLoginIframe () {
    this.iFrame.waitForVisible()
    let loginFrame = this.iFrame.element().value
    browser.frame(loginFrame)
  }

  switchBackFromIframe () {
    browser.frameParent()
  }

  clickConfirmButton () {
    browser.pause(1000)
    console.log(this.openLaterButton)
    if (this.openLaterButton.isVisible()){
      this.openLaterButton.click()
    }
  }

}

export default new OpensLaterPage()
