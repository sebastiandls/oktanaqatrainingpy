import Page from './page'
import { World } from '../World';

World.listOfFood = []
let discount = false

class restaurantPage {

  get menuSearch ()              { return browser.element('#menuSearch') }
  get products ()                { return browser.elements('.product') }
  get productNames ()            { return browser.elements('.productName') }
  get productPrice ()            { return browser.element('.price') }
  get productsAddOrder ()        { return browser.elements('b[title="Agregar a mi pedido"]') }
  get cartProducts ()            { return browser.elements('ul li[data-auto="shopdetails_cart_item"]') }
  get cartProductsName ()        { return browser.elements('.nameWrapper .name') }
  get cartProductsSubtotal ()    { return browser.element('section#subtotal div.price') }
  get cartProductsTotal ()       { return browser.element('section.total.totalContent div.price.total-price') }
  get cartTitle()                { return browser.element('.peyaCard header h4')}
  get optionAmount()             { return browser.element('.peyaCard .productQuantity.dropdown select option') }
  get selectAmount ()            { return browser.elements('.peyaCard .productQuantity.dropdown select') }
  get discountTitle()            { return browser.element('section#discount div.row') }
  get discountPrice()            { return browser.element('section#discountLine div.price') }
  get cart()                     { return browser.element('#cart') }
  

  getMenusUnderMisPedidos(){
    let foods = []
    let texto
    for( let food of this.cartProducts.value ){ 
      texto = browser.elementIdElement(food.value.ELEMENT, this.cartProductsName.selector).getText()
      foods.push(texto)
    }
    return foods
  }


  getSubTotal () {
    this.cartProductsSubtotal.waitForVisible()
    return this.cartProductsSubtotal.getText()
  }
  
  getTotal () {
    this.cartProductsTotal.waitForVisible()
    return this.cartProductsTotal.getText()
  }

  addProductToOrder (food){
    let foods = []
    this.products.value.forEach(function(card){ //Por cada producto
      let nombreProducto = browser.elementIdElement(card.value.ELEMENT, '.productName').getText() //agarra nombre de producto
      if (nombreProducto === food){ //si su nombre es igual al producto de entrada (food)

        browser.elementIdElement(card.value.ELEMENT, 'b[title="Agregar a mi pedido"]').click() //clickea el boton de agregar pedido
        foods.push(food) //ingresa el nombre de la comida agregada al cart a foods
        return false
      }
    })
    World.listOfFood = foods //Crea variable global
  }

  changeAmount(food, number){ 
    browser.pause(800)
    for (var card of this.cartProducts.value){
      let nombreProducto = browser.elementIdElement(card.value.ELEMENT, 'div.nameWrapper span.name').getText()
      if (nombreProducto === food){
        browser.elementIdElement(card.value.ELEMENT, '.productQuantity.dropdown select').click()
        browser.elementIdElement(card.value.ELEMENT, '.productQuantity.dropdown select option').waitForVisible()
        browser.elementIdElement(card.value.ELEMENT, '.productQuantity.dropdown select option[value="' + number + '"] ').click()
      }
    }
  }

  printDiscount() {
    if (this.discountTitle.isExisting()) {
      let num = (this.discountTitle.getText()).substring(4,6)
      console.log("The restaurant has " + num + "% discount")
      discount = true
      return true
    } else {
      console.log("The restaurant has no discount")
      discount = false
      return true
    }
  }

  printPrice() {
    browser.pause(800)
    if (discount){
        console.log("Subtotal: " + this.cartProductsSubtotal.getText())
        console.log("Discount: " + this.discountPrice.getText())
        console.log("Total: " + this.cartProductsTotal.getText())
        return true
    }else {
      console.log("Total without discount: " + this.cartProductsTotal.getText())
      return true
    }
  }

  selectFirstMeal() {
    this.productsAddOrder.value[0].click()
    World.listOfFood = [this.productNames.value[0].getText()]
  }

}
export default new restaurantPage()