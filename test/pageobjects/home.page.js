import Page from './page'
import locationPage from './location.page';
import { World } from '../World';

World.restaurant

class HomePage extends Page {

  get logo ()           { return browser.element('.logo') }
  get ingresarLink ()   { return browser.element('#lnkLogin') }
  get userNameLink ()   { return browser.element('#lnkUserName > div.left.top-link') }
  get ciudadDropdown () { return browser.element('#selectCity_chosen .chosen-single') }
  get addressField ()   { return browser.element('#address') }
  get restaurantField (){ return browser.element('div.optional input.secondary') }
  get buscarButton ()   { return browser.element('#search') }
  get departmentList () { return browser.elements('li.active-result') }

  open () {
    super.open('/')
    this.logo.waitForVisible()
  }

  clickIngresarLink () {
    this.ingresarLink.waitForVisible()
    this.ingresarLink.click()
  }

  clickBuscarButton () {
    this.buscarButton.waitForVisible()
    this.buscarButton.click()
  }

  getUserNameLink () {
    this.userNameLink.waitForVisible()
    return this.userNameLink.getText()
  }

  setDepartment (department) {
    this.ciudadDropdown.click()
    this.departmentList.value[0].waitForVisible()
    let elementText
    for(let i = 0; i < this.departmentList.value.length; i++){
        elementText = this.departmentList.value[i].getText()
        if(elementText === department){
          this.departmentList.value[i].click()
          break
        }
    }
  }

  setAddress (address) {
    this.addressField.waitForVisible()
    this.addressField.setValue(address)
  }

  setRestaurant (restaurant) {
    this.restaurantField.waitForVisible()
    this.restaurantField.setValue(restaurant)
    World.restaurant = restaurant
  }

  searchByAddress (department, address) {
    this.ciudadDropdown.waitForVisible()
    this.setDepartment(department)
    this.setAddress(address)
    this.clickBuscarButton()
    locationPage.clickConfirmButton()
  }
}

export default new HomePage()
