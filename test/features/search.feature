Feature: Search restaurants

  As a user I should be able to search for restaurants
  based on my location and preferences.

  Background:
    Given I am on the home page

  Scenario Outline: Search restaurants by address
    When I select "<department>" as the department
    And I enter "<address>" as the address
    And I search the restaurants
    And I confirm my location
    Then I am taken to the restaurants list
    And I can see restaurants in the list

    Examples:
      | department | address          |
      | Maldonado  | Sarandi 792      |
      | Montevideo | 18 de Julio 1249 |


  Scenario: Search restaurants by address and restaurant name, filter and order
    When I enter "Plaza Cagancha 1234" as the address
    And I enter "La Pasiva" as restaurant
    And I search the restaurants
    And I confirm my location
    And I filter restaurants that accept credit cards
    And I order restaurants by better rating
    And I select the first restaurant
    And I add "Chajá" to the order
    And I change the amount of "Chajá" to "2"
    Then I see all menus under "Mi pedido"
    And I print the restaurant discount
    And I print the price


  Scenario: Search restaurants by address and restaurant name
    When I enter "Plaza Cagancha 1234" as the address
    And I enter "Gourmet" as restaurant
    And I search the restaurants
    And I confirm my location
    Then I am taken to the restaurants list
    And I can see restaurants in the list
    And I can see all restaurants are correct


  Scenario: Search restaurants by address, filter and verify information
    When I enter "Plaza Cagancha 1234" as the address
    And I search the restaurants
    And I confirm my location
    And I filter restaurants that accept credit cards
    And I filter restaurants by "Picadas"
    Then I can see restaurants in the list
    And I print the restaurants information

