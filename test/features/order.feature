Feature: Order a menu

  As a user I should be able to order a menu.

  Background:
    Given I am on a restaurant list for an address
      | department | address          |
      | Montevideo | 18 de Julio 1249 |

  Scenario: Order a menu
    When I select "Facal" restaurant
    And I add "Sándwich caliente con jamón y queso" to the order
    And I add "Ensalada de frutas" to the order
    And I change the amount of "Ensalada de frutas" to "2"
    Then I see all menus under "Mi pedido"
    And I see the subtotal is "$610"
    And I see the total is "$630"

  Scenario: Create an order from an open restaurant
    When I select the first "open" restaurant
    And I add a meal to the order
    Then I see all menus under "Mi pedido"

  Scenario: Create an order from a still not open restaurant
    When I select the first "still not open" restaurant
    And I add a meal to the order
    Then I see all menus under "Mi pedido"

  Scenario: Create an order from a closed restourant
    When I select the first "closed" restaurant
    And I add a meal to the order
    Then I see a message saying the restaurant is closed
